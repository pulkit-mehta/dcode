@extends('layout')
@section('content')
<!-- Reset Password Page -->
<br>
    <a href="/"><input type="button" class="btn btn-dark  left" value="Home"></a>
        
<form action="/resetPassword/{{$user->id}}" method="POST">
    {{csrf_field()}}
    <div class="imgcontainer">
        <h1 class="heading">Set New Password</h1>
    </div>
    <div class="container">
        <label for="password"><b>New Password</b></label>
        <input type="password" placeholder="Enter Password" name="password" id="password" required>
        <label for="password_confirmation"><b>Confirm Password</b></label>
        <input type="password" placeholder="Confirm Password" id="confirmPassword"name="password_confirmation" required>
        <input type="submit"  id="register" value="Change Password" class="btn btn-primary">
        @if($errors->any())
        <br><br>
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    </form>
@stop