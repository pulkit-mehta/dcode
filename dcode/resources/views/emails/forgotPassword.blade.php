@component('mail::message')

Hello {{$user->name}},

Reset password by clicking on the following button.

@component('mail::button', ['url' => url('/resetPassword/'.$user->id )])
RESET
@endcomponent

Thanks,<br>
DCODE Group
@endcomponent
