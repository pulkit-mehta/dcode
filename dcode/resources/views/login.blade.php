@extends('layout')
@section('content')
<!-- Login/ Welcome Page -->
<form action="/login" method="POST">
    {{csrf_field()}}
    <div class="imgcontainer">
        <h1 class="heading">DCODE</h1>
    </div>
    <div class="loginContainer">
        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Enter email" name="email" required>
        <label for="password"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="password" required>
        <button type="submit">Login</button>
        @if (session('loginError'))
        <div class="alert alert-danger">
            {{ session('loginError') }}
        </div>
        @endif
    </div>
    <div class="register"><p><a href="/register" class="register">Sign up </a> /<a href="/forgotPassword" class="register"> Forgot Password?</a></div>
</form>
<!-- Alert msgs for Register/ change password /email sent -->
<div class="loginAlert">
            @if(session('registerMsg')||session('emailMsg')||session('resetPwdMsg'))
            
            <div class=" alert alert-success alertNew">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            @if(session('registerMsg'))
                  {{session('registerMsg')}}
            @endif
            @if(session('emailMsg'))
                  {{session('emailMsg')}}
            @endif
            @if(session('resetPwdMsg'))
                  {{session('resetPwdMsg')}}
            @endif
            </div>
            @endif
      </div>

@stop

@push('scripts')
<script>
      // Get all elements with class="closebtn"
var close = document.getElementsByClassName("closebtn");
var i;

// Loop through all close buttons
for (i = 0; i < close.length; i++) {
  // When someone clicks on a close button
  close[i].onclick = function(){

    // Get the parent of <span class="closebtn"> (<div class="alert">)
    var div = this.parentElement;

    // Set the opacity of div to 0 (transparent)
    div.style.opacity = "0";

    // Hide the div after 600ms (the same amount of milliseconds it takes to fade out)
    setTimeout(function(){ div.style.display = "none"; }, 600);
  }
}
</script>
@endpush
