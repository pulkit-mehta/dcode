@extends('layout')
@section('content')
@include('nav')  
<div>
<!-- Create new Task -->
        <br>
        <a href="/tasks"><input type="button" class="btn btn-dark  left" value="Back"></a>
        
        <h1 class="taskContainer"> Create a New Task</h1>
        <form action="/createTask" method="POST" >
                {{csrf_field()}}
                <div class="container">
                        <div class="form-group">
                                <label for="title"><b>Title</b></label>
                                <input type="text" name="title" class="form-control" placeholder="Title" required>
                        </div>
                        <div class="form-group">
                                <label for="description"><b>Description</b></label>
                                <textarea name="description" class="form-control" placeholder= "Description" required></textarea>
                        </div>
                        <div>      
                                <input type="submit" class="btn btn-success" value="Create">
                                <a href="/tasks"><input type="button" class="btn btn-danger white" value="Cancel"></a>
                        </div>
                        @if($errors->any())
        <br><br>
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
                </div>

        </form>
        
</div>
@stop
