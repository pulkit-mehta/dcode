@extends('layout')
@section('content')
<!-- Register Page -->
<br>
    <a href="/"><input type="button" class="btn btn-dark  left" value="Back"></a>
        
<form action="/register" method="POST">
    {{csrf_field()}}
    <div class="imgcontainer">
        <h1 class="heading">Register</h1>
    </div>
    <div class="container">
        <label for="name"><b>Full Name</b></label>
        <input type="text" placeholder="Enter Full Name" name="name" required>
        <label for="email"><b>Email</b></label>
        <input type="email" placeholder="Enter email" name="email" required>
        <label for="password"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="password" id="password" required>
        <label for="password_confirmation"><b>Confirm Password</b></label>
        <input type="password" placeholder="Confirm Password" id="confirmPassword"name="password_confirmation" required>
        <input type="submit"  id="register" value="Register" class="btn btn-success">
        @if($errors->any())
        <br><br>
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    </form>
@stop