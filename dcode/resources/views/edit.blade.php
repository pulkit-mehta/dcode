@extends('layout')
@section('content')
@include('nav')
<div >
    <!-- Update/ Delete the existing tasks -->
    <br>
    <a href="/tasks"><input type="button" class="btn btn-dark  left" value="Back"></a>
            
    <h1 class="taskContainer">Update Task</h1>
    <div class="container">
        <form action="/tasks/{{$task->id}}" method="POST" class="editButtons">
            {{method_field("PATCH")}}
            {{csrf_field()}}
            <div class="form-group">
                <label for="title"><b>Title</b></label>
                <input type="text"  name="title" placeholder="Title" value="{{$task->title}}" required>
            </div>
            <div class="form-group">
                <label for="description"><b>Description</b></label>
                <textarea name="description" required>{{$task->description}}</textarea>
            </div>
            <input type="submit" value="Update" class="btn btn-primary left">
        </form>
        <form action="/tasks/{{$task->id}}" method="POST" class="editButtons">
            {{method_field("DELETE")}}
            {{csrf_field()}}
            <input type="submit" value="Delete" class="btn btn-danger left">
        </form>
        @if($errors->any())
        <br><br>
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@stop
