@extends('layout')
@section('content')
<!-- Forgot Password Page -->
<br>
    <a href="/"><input type="button" class="btn btn-dark  left" value="Back"></a>
        
<form action="/forgotPassword" method="POST">
    {{csrf_field()}}
    <div class="imgcontainer">
        <h1 class="heading">Reset Password</h1>
    </div>
    <div class="container">
        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Enter email" name="email" required>
        <input type="submit"  id="register" value="Send Link" class="btn btn-success">
        @if($errors->any())
        <br><br>
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    </form>
@stop