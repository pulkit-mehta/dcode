@extends('layout')
@section('content')
@include('nav')
<div>
      <h1 class="taskContainer">Task System</h1>
      <!-- Alert msgs for update / create /delete tasks -->
      <div class="alertMsg">
            @if(session('createMsg')||session('updateMsg')||session('deleteMsg'))
            
            <div class=" alert alert-success alertNew">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            @if(session('createMsg'))
                  {{session('createMsg')}}
            @endif
            @if(session('updateMsg'))
                  {{session('updateMsg')}}
            @endif
            @if(session('deleteMsg'))
                  {{session('deleteMsg')}}
            @endif
            </div>
            @endif
      </div>
      <!-- Add a new task -->
      <form action="/newTask" method="GET" class="right">
            {{csrf_field()}}
            <input type="submit" class="btn btn-success" value="Add Task" />
      </form>
      <br>
      <!-- Display all the tasks -->
      <form method="GET" >
            {{csrf_field()}}
            <table class="table tab table-responsive">
            <thead class="thead-dark">
                  <tr>
                  <th scope="col">#</th>
                  <th scope="col">Title</th>
                  <th scope="col">Description</th>
                  <th scope="col">Modify</th>
                  </tr>
            </thead>
            <tbody>
                  <?php $i = 0 ?>
                  @foreach ($tasks as $task)
                  <?php $i++ ?>
                  <tr>
                  <td>
                        {{ $i}}
                  </td>
                  <td>
                        {{ $task->title }}
                  </td>
                  <td>
                        {{$task->description}}
                  </td>
                  <td>
                        <input type="submit" class="btn btn-primary" value="Edit" formaction="{{url('tasks/'.$task->id)}} ">
                  </td>
                  <tr>
                  @endforeach
            </tbody>
            </table>
      </form>
</div>

@stop

@push('scripts')
<script>
      // Get all elements with class="closebtn"
var close = document.getElementsByClassName("closebtn");
var i;

// Loop through all close buttons
for (i = 0; i < close.length; i++) {
  // When someone clicks on a close button
  close[i].onclick = function(){

    // Get the parent of <span class="closebtn"> (<div class="alert">)
    var div = this.parentElement;

    // Set the opacity of div to 0 (transparent)
    div.style.opacity = "0";

    // Hide the div after 600ms (the same amount of milliseconds it takes to fade out)
    setTimeout(function(){ div.style.display = "none"; }, 600);
  }
}
</script>
@endpush
