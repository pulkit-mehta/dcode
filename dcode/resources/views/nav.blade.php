<html>
    <head>
      <title>DCODE</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
    </head>
    <body>
        <!-- Navbar -->
        <nav class="navbar navbar-dark bg-dark ">
            <a class="navbar-brand" href="/tasks"><h2>DCODE</h2></a>
            <form action="/logout" method="POST" class="bottomZero">
            {{csrf_field()}}
                @if(session('user')!==null)
                <div class="username">
                    <img src="{{asset('images/user.png')}}" class="logo"><span class="username">{{session('user')}}</span></img>
                @endif
                <input type="submit" value="Logout" class="right btn btn-warning">
                </div>
            </form>
        </nav>
    </body>
</html>    