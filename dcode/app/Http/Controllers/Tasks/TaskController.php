<?php

namespace App\Http\Controllers\Tasks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Task;
class TaskController extends Controller
{
/**Views the Tasks list */
    public function index()
  {
    $tasks= \App\Task::all();
    return view('tasks', compact('tasks'));
  }
/** Stores the new task after validating it */
  public function store(Request $request)
  {
    $data=$request;
    $this->validateNewTask($request);
    $task= new Task();
    $task->title= request('title');
    $task->description= request('description');
    $task->save();
    return redirect('tasks')->with('createMsg', 'Task created successfully!');
  }
/**Views the create task page */
  public function createTask()
  {
    return view('create');
  }
/**Views the edit page  */
  public function edit($id)
  {
    $task= Task::find($id);
    return view('edit', compact('task'));
  }
/**Updates the task selected after validating the user input */
  public function update($id, Request $request)
  {
    $this->validateTask($request);
    $task= Task::find($id);
    $task->title= $request->title;
    $task->description=$request->description;
    $task->save();
    return redirect('tasks')->with('updateMsg', 'Task updated successfully!');
  }
/**Deletes the task */
  public function delete($id)
  {
    Task::find($id)->delete();
    return redirect('tasks')->with('deleteMsg', 'Task deleted successfully!');
  }
/**Validates the task as per the rules */
  public function validateTask(Request $request)
  {
    $request->validate([
      'title'=>"required|max:200|string",
      'description'=>"required|max:500|string"]);
  }
  public function validateNewTask($data)
  {
    $data->validate([
      'title'=>"required|max:200|string",
      'description'=>"required|max:500|string"]);
  }
}