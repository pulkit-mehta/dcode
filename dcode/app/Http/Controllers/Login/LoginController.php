<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
  /**Views the login page */
  public function index()
  {
      return view('login');
  }
//**Logs in user after validating and checking credentials */
  public function login(Request $request)
  {
    $this->validateCredentials($request);
    $user= $this->checkCredentials($request); 
    if(count($user)==1)
    {
      session()->flush();
      session(['user'=> $user[0]->name]);
      return redirect('tasks');
    }
    else
    {
      return redirect('/')->with('loginError', 'Wrong email or password!');
    }
  }
  /**Validation of the user input */
  public function validateCredentials(Request $request)
  {
    $validatedData =$request->validate([
      'email' => 'required|max:255|email',
      /**email with max length 255 */
      'password' => 'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$*#%]).*$/',
      /**The password contains characters from at least three of the following five categories:
          English uppercase characters (A – Z)
          English lowercase characters (a – z)
          Base 10 digits (0 – 9)
          Non-alphanumeric (For example: !, $, #, * or %)
          Unicode characters*/
  ]);

  }
  /**checks the credentials in the database */
  public function checkCredentials(Request $request)
  {
    $user = DB::select('select name from users where email = :email && password= :password', ['email'=>$request->email,'password'=>$request->password]);
    return $user;
  }
/**Logs out the user and flushes the session */
  public function logout()
  {
    session()->flush();
    return redirect('/')->with('logoutMsg', 'Logout successfull!');
  }


}
