<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class RegisterController extends Controller
{
  /**Views the register page */
  public function index()
  {
      return view('register');
  }

  /**Register the guest */
  public function register(Request $request)
  {
      $this->validateCredentials($request);
      $user= new User();
      $user->name= request('name');
      $user->email= request('email');
      $user->password= request('password');
      $user->save();
      return redirect('/')->with('registerMsg', 'Registered successfully!');
  }


/**Validation of the user input */
public function validateCredentials(Request $request)
{
  $validatedData =$request->validate([
    'name'=>'required|max:50|string',
    'email' => 'required|max:255|email',
    /**email with max length 255 */
    'password' => 'required|confirmed|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$*#%]).*$/',
    /**The password contains characters from at least three of the following five categories:
        English uppercase characters (A – Z)
        English lowercase characters (a – z)
        Base 10 digits (0 – 9)
        Non-alphanumeric (For example: !, $, #, * or %)
        Unicode characters*/
  ],
  [
      'password.regex'=>'The password contains characters from at least three of the following five categories:
      English uppercase characters (A – Z)
      English lowercase characters (a – z)
      Base 10 digits (0 – 9)
      Non-alphanumeric (For example: !, $, #, * or %)
      Unicode characters '
  ]);

}

}
