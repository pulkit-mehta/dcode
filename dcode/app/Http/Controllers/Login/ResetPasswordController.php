<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class ResetPasswordController extends Controller
{
/* Resets password */
public function resetpassword($id,Request $request)
{
    $this->validatePassword($request);
    $user=User::find($id);
    $user->password= request('password');
    $user->save();
    return redirect('/')->with('resetPwdMsg', 'Password changed successfully!');
}
/** Shows a view to change password */
public function changePassword($id)
{
    $user=User::find($id);
    return view('resetPassword', compact('user'));
}

/**Validation of the user input */
public function validatePassword(Request $request)
{
  $validatedData =$request->validate([
    'password' => 'required|confirmed|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$*#%]).*$/',
    /**The password contains characters from at least three of the following five categories:
        English uppercase characters (A – Z)
        English lowercase characters (a – z)
        Base 10 digits (0 – 9)
        Non-alphanumeric (For example: !, $, #, * or %)
        Unicode characters*/
  ],
  [
      'password.regex'=>'The password contains characters from at least three of the following five categories:
      English uppercase characters (A – Z)
      English lowercase characters (a – z)
      Base 10 digits (0 – 9)
      Non-alphanumeric (For example: !, $, #, * or %)
      Unicode characters '
  ]);

}
}