<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Mail;
use App\Mail\ForgotPassword;
class ForgotPasswordController extends Controller
{
  /**Views the forgot password page */
  public function index()
  {
      return view('forgotPassword');
  }

  /**Reset the password */
  public function reset(Request $request)
  {
      $this->validateEmail($request);
      $user= User::where('email',$request->email)->first();
      Mail::to($request->email)->send(
          new ForgotPassword($user)
      );
      return redirect('/')->with('emailMsg', 'We have e-mailed your password reset link!');
  }


/**Validation of the user input */
public function validateEmail(Request $request)
{
  $validatedData =$request->validate([
    'email' => 'required|max:255|email',
  ]);

}


}
