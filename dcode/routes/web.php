<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Login\LoginController@index');
Route::post('/login', 'Login\LoginController@login');
Route::post('/logout', 'Login\LoginController@logout');

Route::get('/register','Login\RegisterController@index');
Route::post('/register','Login\RegisterController@register');

Route::get('/forgotPassword','Login\ForgotPasswordController@index');
Route::post('/forgotPassword','Login\ForgotPasswordController@reset');

Route::get('/resetPassword/{id}','Login\ResetPasswordController@changePassword');
Route::post('/resetPassword/{id}','Login\ResetPasswordController@resetPassword');

Route::middleware('isLoggedIn')->group(function () {
    Route::get('/tasks','Tasks\TaskController@index');
    Route::post('/createTask','Tasks\TaskController@store');
    Route::get('/newTask','Tasks\TaskController@createTask');

    Route::get('/tasks/{id}','Tasks\TaskController@edit');
    Route::patch('/tasks/{id}','Tasks\TaskController@update');
    Route::delete('/tasks/{id}','Tasks\TaskController@delete');
});
