Task System
--------------------
__Development Environment__:
Apache, MySQL and PHP with Laravel framework

__User__ can do the following:
1. Login/logout to the Task system.
2. View all the tasks.
3. Create new tasks.
4. Update and delete tasks.

__To Run the project__
1. Clone the project 
2. Setup the environment for the laravel project
3. Add the shared ENV file
4. Using Migration and seeding migrate tables and get pre-poulated data 